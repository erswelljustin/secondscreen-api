<?php
	$i = 23;
	$date = date('d-m-Y');
	
	$to = 'justin@createdm.com';
	$subject = 'Server Status Update - EPG Ingest';
	
	$headers = "From: server@2ndscreen.tv\r\n";
	$headers .= "Reply-To: no-reply@2ndscreen.tv\r\n"; 
	$headers .= "MIME-Version: 1.0\r\n";
	$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
	$message = '<html><head><meta content="telephone=no" name="format-detection"><meta content="date=no" name="format-detection"></head><body style="font-family: Helvetica">';
	$message .= '<div style="width: 60%; margin: 0 auto; text-align: center;">';
	$message .= '<img src="http://api.2ndscreen.tv/v1/data/2S.png" style="float-left; width: 44px"><h3>Server Status Update - EPG Ingest</h3>';
	$message .= '<p>The server successfully ingested '.$i.' EPG records</p>';
	$message .= '<hr>';
	$message .= '<p style="color: #999; font-size: 10px">'.$date.'</p>';
	$message .= "</body></html>";
	
	mail($to, $subject, $message, $headers);
?>						