<?php
	date_default_timezone_set('Europe/London');
	
	$now = microtime(true);
	$APPLICATION_ID = "HEvOP4Gp4dJx8ttEHUM7SBdkZrrpJrGgWbF6DtkY";
	$REST_API_KEY = "9xFlE3IxFuXg3zAw9B0jG7MaKYwbGVHHZ2SrRxLE";
	
	$db = getConnection();
	
	$expVODItems = $db->query("SELECT * FROM `meta_episodes` WHERE FROM_UNIXTIME(`epis_expiry`) BETWEEN FROM_UNIXTIME('$now') AND FROM_UNIXTIME('$now') + INTERVAL 24 HOUR ")->fetchAll(PDO::FETCH_OBJ);
	$vodCount = count($expVODItems);
	
	if($vodCount > 0) {
		foreach($expVODItems as $e) {
			$vodId = $e->epis_episode_id;
			$vodPID = $e->epis_programme_id;
			$vodTitle = $e->epis_series_title.' - '.$e->epis_title;
			$vodExpires = gmdate('g:ia d/m/y', $e->epis_expiry);
			$channel = 'vod_expiry';
			
			$waList = $db->query("SELECT `wali_user_id` FROM `app_watch_list` WHERE `wali_programme_id`='$vodPID' AND `wali_status`=1")->fetchAll(PDO::FETCH_OBJ);			
			
			foreach($waList as $w) {
				$waUserId = $w->wali_user_id;
				
				$waStatus = $db->query("SELECT `wast_status` FROM `app_watch_status` WHERE `wast_episode_id`='$vodId' AND `wast_user_id`='$waUserId'")->fetchColumn();
				if($waStatus==NULL || $waStatus==0) {
					
					$uDevice = $db->query("SELECT `devi_device_token` FROM `app_device` WHERE `devi_user_id`='$waUserId' GROUP BY `devi_device_token`")->fetchAll(PDO::FETCH_OBJ);
			
					$devArr = array();
					foreach ($uDevice as $result) {
					  array_push($devArr, $result->devi_device_token);
					}
					
					$alert = $vodTitle." is expiring soon - ".$vodExpires;
					
					$url = 'https://api.parse.com/1/push';
					$data = array(
						"where" => array(
							"deviceType" => "ios",
							"channels" => array(
								'$in' => array(
									$channel
								)
							),
							"deviceToken" => array(
								'$in' => $devArr
							)
						),
						'data' => array(
							'alert' => $alert,
							'action' => array(
								'entity_id' => $vodId,
								'programme_id' => $vodPID,
								'alert_type' => 1
							),
						),
					);
			        $_data = json_encode($data);
			        $headers = array(
			            'X-Parse-Application-Id: ' . $APPLICATION_ID,
			            'X-Parse-REST-API-Key: ' . $REST_API_KEY,
			            'Content-Type: application/json',
			            'Content-Length: ' . strlen($_data),
			        );
			
			        $curl = curl_init($url);
			        curl_setopt($curl, CURLOPT_POST, 1);
			        curl_setopt($curl, CURLOPT_POSTFIELDS, $_data);
			        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
			        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
			        $response = curl_exec($curl);		
		
					$file = '/var/www/v1/data/parse/logs/vod_expiry_log.txt';
					$data = "Push notifcation sent to user - '$userId' for VOD - '$vodId' - @ '$now' - Completed Successfully --- Response Text: ".$response.PHP_EOL;
					file_put_contents($file, $data, FILE_APPEND);
					
				} else {
					$file = '/var/www/v1/data/parse/logs/vod_expiry_log.txt';
					$data = "No records were found to send to Parse - @ '$now' - Wating for next scheduled run".PHP_EOL;
					file_put_contents($file, $data, FILE_APPEND);
				}
			}
		}
	}
	
	$db = null;
	
	function getConnection() {
		$dbhost 		= "db.2ndscreen.tv";//NEW
		$dbuser 		= "SecondScreen";
		$dbpass 		= "9pEPstpHdfe9WTy";
		$dbname			= "SecondScreen";
		$options 		= array(
		    PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
		); 		
	    $dbh = new PDO("mysql:host=$dbhost;dbname=$dbname", $dbuser, $dbpass, $options);
	    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	    return $dbh;			
	}
?>