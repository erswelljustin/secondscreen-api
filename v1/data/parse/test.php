<?php
$APPLICATION_ID = "HEvOP4Gp4dJx8ttEHUM7SBdkZrrpJrGgWbF6DtkY";
$REST_API_KEY = "9xFlE3IxFuXg3zAw9B0jG7MaKYwbGVHHZ2SrRxLE";

date_default_timezone_set('UTC');

$url = 'https://api.parse.com/1/push';
$data = array(
	"where" => array(
		"deviceType" => "ios",
		"channels" => array(
			'$in' => array(
				"vod_new_episode"
			)
		),
		"deviceToken" => array(
			'$in' => array(
				//Comma Seperated List of Device Tokens
				"bda6bc06010442db3a48e13383edb275d0131c5d2eade2b761d906ba7a9d12d3",
				"6952bff9db3ad9306e8dcc457791131ae80133b043b11d8f67c064930edcd5e7"
			)
		)
	),
	'data' => array(
        'alert' => 'A new episode of Countryfile has just been added',
		'action' => array(
			'entity_id' => 9353,
			'programme_id' => 219711,
			'alert_type' => 2
		),
    ),
);
$_data = json_encode($data);
$headers = array(
    'X-Parse-Application-Id: ' . $APPLICATION_ID,
    'X-Parse-REST-API-Key: ' . $REST_API_KEY,
    'Content-Type: application/json',
    'Content-Length: ' . strlen($_data),
);

$curl = curl_init($url);
curl_setopt($curl, CURLOPT_POST, 1);
curl_setopt($curl, CURLOPT_POSTFIELDS, $_data);
curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
$response = curl_exec($curl);
echo json_encode($response);
?>