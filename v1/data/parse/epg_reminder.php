<?php
	date_default_timezone_set('Europe/London');
	
	$now = date('Y-m-d H:i:s');
	$APPLICATION_ID = "HEvOP4Gp4dJx8ttEHUM7SBdkZrrpJrGgWbF6DtkY";
	$REST_API_KEY = "9xFlE3IxFuXg3zAw9B0jG7MaKYwbGVHHZ2SrRxLE";
	
	$db = getConnection();
	
	$epgNotifications = $db->query("SELECT * FROM `app_notification` WHERE noti_type=0 AND noti_status=1 AND noti_run_date BETWEEN '$now' AND '$now' + INTERVAL 5 MINUTE")->fetchAll(PDO::FETCH_OBJ);
	$epgNoteCount = count($epgNotifications);
	
	if($epgNoteCount > 0) {
		foreach($epgNotifications as $e) {

			$epgId = $e->noti_entity_id;
			$epgStart = $e->noti_run_date;
			$userId = $e->noti_user_id;
			$channel = 'epg_reminder';
			
			$eRecord = $db->query("SELECT `epg_display_title` FROM `app_epg` WHERE `epg_epg_id`='$epgId'")->fetchColumn();
			$uDevice = $db->query("SELECT `devi_device_token` FROM `app_device` WHERE `devi_user_id`='$userId' GROUP BY `devi_device_token`")->fetchAll(PDO::FETCH_OBJ);
			
			$devArr = array();
			foreach ($uDevice as $result) {
			  array_push($devArr, $result->devi_device_token);
			}
			
			$alert = $eRecord.' is about to start ('.date('h:i', strtotime($epgStart)).')';
			
			$url = 'https://api.parse.com/1/push';
		    $data = array(
				"where" => array(
					"deviceType" => "ios",
					"channels" => array(
						'$in' => array(
							$channel
						)
					),
					"deviceToken" => array(
						'$in' => $devArr
					)
				),
	    		'data' => array(
	            	'alert' => $alert,
					'action' => array(
						'entity_id' => $epgId,
						'alert_type' => 0
					),
	            ),
			);
	        $_data = json_encode($data);
	        $headers = array(
	            'X-Parse-Application-Id: ' . $APPLICATION_ID,
	            'X-Parse-REST-API-Key: ' . $REST_API_KEY,
	            'Content-Type: application/json',
	            'Content-Length: ' . strlen($_data),
	        );

	        $curl = curl_init($url);
	        curl_setopt($curl, CURLOPT_POST, 1);
	        curl_setopt($curl, CURLOPT_POSTFIELDS, $_data);
	        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
	        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	        $response = curl_exec($curl);		
			
			$file = '/var/www/v1/data/parse/logs/epg_reminder_log.txt';
			$data = "Push notifcation sent to user - '$userId' for EPG - '$epgId' - @ '$now' - Completed Successfully --- Response Text: ".$response.PHP_EOL;
			file_put_contents($file, $data, FILE_APPEND);
			
			$updated = microtime(true);
			
			$db->exec("UPDATE `app_notification` SET `noti_status`=0, `noti_updated`='$updated' WHERE `noti_entity_id`='$epgId' AND `noti_user_id`='$userId'");
		}
	} else {
		$file = '/var/www/v1/data/parse/logs/epg_reminder_log.txt';
		$data = "No records were found to send to Parse - @ '$now' - Wating for next scheduled run".PHP_EOL;
		file_put_contents($file, $data, FILE_APPEND);
	}

	$db = null;
	
	function getConnection() {
		$dbhost 		= "db.2ndscreen.tv";//NEW
		$dbuser 		= "SecondScreen";
		$dbpass 		= "9pEPstpHdfe9WTy";
		$dbname			= "SecondScreen";
		$options 		= array(
		    PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
		); 		
	    $dbh = new PDO("mysql:host=$dbhost;dbname=$dbname", $dbuser, $dbpass, $options);
	    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	    return $dbh;			
	}
?>