<?php
function getUKTimeZone() {
	// declare some start variables 
	$ThisYear = (date("Y")); 
	$MarStartDate = ($ThisYear."-03-25"); 
	$OctStartDate = ($ThisYear."-10-25"); 
	$MarEndDate = ($ThisYear."-03-31"); 
	$OctEndDate = ($ThisYear."-10-31"); 

	//work out the Unix timestamp for 1:00am GMT on the last Sunday of March, when BST starts 
	while ($MarStartDate <= $MarEndDate) 
	{ 
	$day = date("l", strtotime($MarStartDate)); 
	if ($day == "Sunday"){ 
	$BSTStartDate = ($MarStartDate); 
	} 
	$MarStartDate++; 
	} 
	$BSTStartDate = (date("U", strtotime($BSTStartDate))+(60*60)); 
	//work out the Unix timestamp for 1:00am GMT on the last Sunday of October, when BST ends 
	while ($OctStartDate <= $OctEndDate) 
	{ 
	$day = date("l", strtotime($OctStartDate)); 
	if ($day == "Sunday"){ 
	$BSTEndDate = ($OctStartDate); 
	} 
	$OctStartDate++; 
	} 
	$BSTEndDate = (date("U", strtotime($BSTEndDate))+(60*60)); 

	//Check to see if we are now in BST 
	$now = mktime(); 
	if (($now >= $BSTStartDate) && ($now <= $BSTEndDate)){ 
		return 1;
	} 
	else { 
		return 0;
	} 
}
?>