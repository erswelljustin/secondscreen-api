<?php
	if(!defined('APPLICATION_PATH')) define('APPLICATION_PATH',  dirname(__FILE__));

	date_default_timezone_set('Europe/London');
	$APPLICATION_ID = "HEvOP4Gp4dJx8ttEHUM7SBdkZrrpJrGgWbF6DtkY";
	$REST_API_KEY = "9xFlE3IxFuXg3zAw9B0jG7MaKYwbGVHHZ2SrRxLE";
	
	$db = getConnection();
	$nextIngest = $db->query("SELECT `epis_vod_id` FROM `meta_episodes` GROUP BY `epis_vod_id` ORDER BY `epis_vod_id`+0 DESC LIMIT 1")->fetchColumn();
	$eb = null;
	
	//Include the IMage Tools
	include('imgTools.php');

	$call = "http://api.vizimo.com/v1/vodEpisodes?since=".$nextIngest;
	
	$username = 'cdm';
	$password = 'v1z1m0';
	
	$curl_session = curl_init($call);
	
	$curl_options = array(
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_HTTPHEADER => array(
			'Accept: application/json',
			'Content-Type: application/json'
		),
		CURLOPT_USERPWD => $username.':'.$password
	);
	
	curl_setopt_array($curl_session, $curl_options);
	$curl_response = curl_exec($curl_session);
	$data = json_decode($curl_response);
	
	$t = count($data);
	echo 'Total records = '.$t.PHP_EOL;
	$i = 0;
	foreach($data as $bc) {
		$thumb = '';
		if(array_key_exists('thumb', $bc)) {
					
			$url = 'http://thumbs.vizimo.com/tv/'.$bc->thumb.'-1024x576.jpg';
			$urlAlt = 'http://thumbs.vizimo.com/tv/'.$bc->thumb.'.jpg';
			
			$extension 	= 'jpg';
			$fName 		= rand_string(24);
			$finalName 	= $fName.'.'.$extension;
			$img 		= APPLICATION_PATH.'/tmp/'.$finalName;
			
			if(get_http_response_code($url) != "404"){
			   file_put_contents($img, file_get_contents($url));
			}else{
			   file_put_contents($img, file_get_contents($urlAlt));
			}			

			//Convert & Save Images
			$gsr = gridBox(946,710,'gsr',$fName,100);
			$gs = gridBox(473,355,'gs',$fName,50);
			$gcr = gridBox(452,280,'gcr',$fName,100);
			$gc = gridBox(226,140,'gc',$fName,50);
			$tcr = popupImage(200,140,'tcr',$fName);
			$ppa = popupImage(690,352,'ppa',$fName);
			$ppb = popupImage(345,176,'ppb',$fName);
			$ppc = popupImage(640,352,'ppc',$fName);

			$thumb = $finalName;
		}
		
		$episodeNum = (array_key_exists('episode_number', $bc) ? $bc->episode_number : 0);
		$seasonNum = (array_key_exists('season_number', $bc) ? $bc->season_number : 0);
		
		$ios_url = (array_key_exists('ios_url', $bc) ? $bc->ios_url : '');
		$play_url = (array_key_exists('play_url', $bc) ? $bc->play_url : '');

		$displayTitle 	= html_entity_decode($bc->display_title, ENT_QUOTES | ENT_HTML5);
		$programmeId 	= $bc->programme_id;
		$episodeId 		= $bc->episode_id;
		$vod_id			= $bc->vod_id;
		$expires		= strtotime($bc->expires);
		$shortInfo 		= html_entity_decode($bc->short_info, ENT_QUOTES | ENT_HTML5);
		$info 			= html_entity_decode($bc->info, ENT_QUOTES | ENT_HTML5);
		$genre 			= $bc->genre;
		$channel 		= $bc->bcast_channel;
		$bcastDate 		= strtotime($bc->bcast_start);
		
		if(array_key_exists('episode_title', $bc)) {
			$episodeTitle = html_entity_decode($bc->episode_title, ENT_QUOTES | ENT_HTML5);
		} elseif(!$seasonNum==0 && !$episodeNum==0) {
			$episodeTitle = 'Season '.$seasonNum.' Episode '.$episodeNum;
		} elseif($episodeNum==0 && $seasonNum==0) {
			$dbDate = date('l jS F Y', strtotime($bc->bcast_start));
			$episodeTitle = $dbDate;
		} else {
			$episodeTitle = '';
		}
		
		$updated		= microtime(true);
		
		$sql = "INSERT INTO `meta_episodes` (`epis_genre`, `epis_channel`, `epis_title`, `epis_series_title`, `epis_series`, `epis_number`, `epis_broadcast_date`, `epis_expiry`, `epis_synopsis`, `epis_synopsis_long`, `epis_image_url`, `epis_vod_id`, `epis_web_url`, `epis_inapp_url`, `epis_status`, `epis_atlas_id`, `epis_programme_id`,`epis_updated`) VALUES (:genre,:channel,:episodeTitle,:displayTitle,:seasonNum,:episodeNum,:bcastDate,:expires,:shortInfo,:info,:thumb,:vod_id,:play_url,:ios_url,0,:episodeId,:programmeId,:updated) ON DUPLICATE KEY UPDATE `epis_updated`=:updated, `epis_expiry`=:expires";	
		try {
			$db = getConnection();
			$db->exec("SET NAMES 'utf8mb4' COLLATE 'utf8mb4_unicode_ci';");
			$stmt = $db->prepare($sql);
			$stmt->bindParam(":genre", $genre);
			$stmt->bindParam(":channel", $channel);
			$stmt->bindParam(":episodeTitle", $episodeTitle);
			$stmt->bindParam(":displayTitle", $displayTitle);
			$stmt->bindParam(":seasonNum", $seasonNum);
			$stmt->bindParam(":episodeNum", $episodeNum);
			$stmt->bindParam(":bcastDate", $bcastDate);
			$stmt->bindParam(":expires", $expires);
			$stmt->bindParam(":shortInfo", $shortInfo);
			$stmt->bindParam(":info", $info);
			$stmt->bindParam(":thumb", $thumb);
			$stmt->bindParam(":vod_id", $vod_id);
			$stmt->bindParam(":play_url", $play_url);
			$stmt->bindParam(":ios_url", $ios_url);
			$stmt->bindParam(":episodeId", $episodeId);
			$stmt->bindParam(":programmeId", $programmeId);
			$stmt->bindParam(":updated", $updated);
			$stmt->execute();
			$db = null;
			$i++;
			echo 'Record added - '.$i.PHP_EOL;
		} catch(PDOException $e) {
			echo $e->getMessage().PHP_EOL;		
		}
		
		// Check and send a push notification to the relevant users
		$channel = 'vod_new_episode';
		$db = getConnection();
		$waliUsers = $db->query("SELECT `wali_user_id` FROM `app_watch_list` WHERE `wali_programme_id`='$programmeId' AND `wali_status`=0")->fetchAll(PDO::FETCH_OBJ);
		if(!$waliUsers==NULL || !$waliUsers==0) {
			
			$uDevice = $db->query("SELECT `devi_device_token` FROM `app_device` WHERE `devi_user_id`='$waUserId' GROUP BY `devi_device_token`")->fetchAll(PDO::FETCH_OBJ);
	
			$devArr = array();
			foreach ($uDevice as $result) {
			  array_push($devArr, $result->devi_device_token);
			}
			
			$alert = "A new episode of ".$displayTitle." has just been added";
			
			$url = 'https://api.parse.com/1/push';
			$data = array(
				"where" => array(
					"deviceType" => "ios",
					"channels" => array(
						'$in' => array(
							$channel
						)
					),
					"deviceToken" => array(
						'$in' => $devArr
					)
				),
				'data' => array(
					'alert' => $alert,
					'action' => array(
						'entity_id' => $episodeId,
						'programme_id' => $programmeId,
						'alert_type' => 2
					),
				),
			);
	        $_data = json_encode($data);
	        $headers = array(
	            'X-Parse-Application-Id: ' . $APPLICATION_ID,
	            'X-Parse-REST-API-Key: ' . $REST_API_KEY,
	            'Content-Type: application/json',
	            'Content-Length: ' . strlen($_data),
	        );
	
	        $curl = curl_init($url);
	        curl_setopt($curl, CURLOPT_POST, 1);
	        curl_setopt($curl, CURLOPT_POSTFIELDS, $_data);
	        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
	        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	        $response = curl_exec($curl);		

			$file = '/var/www/v1/data/parse/logs/vod_new_episode_log.txt';
			$data = "Push notifcation sent to user - '$userId' for VOD - '$vodId' - @ '$now' - Completed Successfully --- Response Text: ".$response.PHP_EOL;
			file_put_contents($file, $data, FILE_APPEND);
			
		} else {
			$file = '/var/www/v1/data/parse/logs/vod_new_episode_log.txt';
			$data = "No records were found to send to Parse - @ '$now' - Wating for next scheduled run".PHP_EOL;
			file_put_contents($file, $data, FILE_APPEND);
		}
		$db = null;		
	}
	// close session
	curl_close($curl_session);
	
	//Email here…
	//sendEmail($i);
	
	function rand_string( $length ) {
		$chars = "abcdefghijklmnopqrstuvwxyz0123456789";	
		$str = '';
		
		$size = strlen( $chars );
		for( $i = 0; $i < $length; $i++ ) {
			$str .= $chars[ rand( 0, $size - 1 ) ];
		}
	
		return $str;
	}
	
	function get_http_response_code($url) {
	    $headers = get_headers($url);
	    return substr($headers[0], 9, 3);
	}
	
	// function sendEmail($i) {
	// 	$date = date('d-m-Y');
	// 	
	// 	$to = 'justin@createdm.com';
	// 	$subject = 'Server Status Update - VOD Ingest';
	// 	
	// 	$headers = "From: server@2ndscreen.tv\r\n";
	// 	$headers .= "Reply-To: no-reply@2ndscreen.tv\r\n"; 
	// 	$headers .= "MIME-Version: 1.0\r\n";
	// 	$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
	// 	$message = '<html><head><meta content="telephone=no" name="format-detection"><meta content="date=no" name="format-detection"></head><body style="font-family: Helvetica">';
	// 	$message .= '<div style="width: 60%; margin: 0 auto; text-align: center;">';
	// 	$message .= '<img src="http://api.2ndscreen.tv/v1/data/2S.png" style="float-left; width: 44px"><h3>Server Status Update - VOD Ingest</h3>';
	// 	$message .= '<p>The server successfully ingested '.$i.' VOD records</p>';
	// 	$message .= '<hr>';
	// 	$message .= '<p style="color: #999; font-size: 10px">'.$date.'</p>';
	// 	$message .= "</body></html>";
	// 	
	// 	mail($to, $subject, $message, $headers);		
	// }
	
	// PDO Function
	function getConnection() {
		$dbhost 		= "db.2ndscreen.tv";//NEW
		$dbuser 		= "SecondScreen";
		$dbpass 		= "9pEPstpHdfe9WTy";
		$dbname		= "SecondScreen";
		$options 		= array(
		    PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
		); 		
	    $dbh = new PDO("mysql:host=$dbhost;dbname=$dbname", $dbuser, $dbpass, $options);
	    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	    return $dbh;			
	}

?>
						