<?php
	if(!defined('APPLICATION_PATH')) define('APPLICATION_PATH',  dirname(__FILE__));

	date_default_timezone_set('Europe/London');
	
	//Include the GD Resize Function
	include('imgToolsEPG.php');
	
	$srcFile = simplexml_load_file('/home/paftp/1107_tvdata.xml');
	
	$channelData = $srcFile->listings_date->channel_data;
	foreach($channelData as $c) {
		$cName = $c['name'];
		$cId = $c['channel_id'];
		if($cId==412) 		{ $cId = 1395; } 	// BBC1
		elseif($cId==420) 	{ $cId = 1414; } 	// BBC2
		elseif($cId==935) 	{ $cId = 1432; }	// BBC3
		elseif($cId==742) 	{ $cId = 1433; }	// BBC4
		elseif($cId==85) 	{ $cId = 1436; }	// BBC News
		elseif($cId==734) 	{ $cId = 1456; }	// CBeebies
		elseif($cId==49) 	{ $cId = 1540; }	// ITV
		elseif($cId==451) 	{ $cId = 1558; }	// ITV2
		elseif($cId==1065) 	{ $cId = 1560; }	// ITV3
		elseif($cId==1174)	{ $cId = 1562; }	// ITV4
		elseif($cId==1204) 	{ $cId = 1472; }	// CITV
		elseif($cId==53) 	{ $cId = 1463; }	// C4
		elseif($cId==1167) 	{ $cId = 1573; }	// More 4
		elseif($cId==605) 	{ $cId = 1502; }	// E4
		elseif($cId==1684) 	{ $cId = 2020; }	// 4 Seven
		elseif($cId==220) 	{ $cId = 1515; }	// Film 4
		elseif($cId==54) 	{ $cId = 1517; }	// C5
		elseif($cId==1289) 	{ $cId = 1518; }	// 5 *
		elseif($cId==1290) 	{ $cId = 1934; }	// S USA
		
		$pData = $c->prog_data;
		$num = 0;
		foreach($pData as $p) {
			$num++;
			$thumb = '';
			
			$url = '/home/paftp/images/'.$p->pictures->pictureUsage;
			
			if (file_exists($url)) {
				$extension 	= 'jpg';
				$fName 		= rand_string(24);
				$finalName 	= $fName.'.'.$extension;
				$img 		= APPLICATION_PATH.'/tmp/'.$finalName;
				file_put_contents($img, file_get_contents($url));
			
				//Convert & Save Images
				$gcr 	= gridBox(452,280,'gcr',$fName,100);
				$gc 	= gridBox(226,140,'gc',$fName,50);
				$tcr 	= popupImage(200,140,'tcr',$fName);
				$ppa 	= popupImage(690,352,'ppa',$fName);
				$ppb 	= popupImage(345,176,'ppb',$fName);
				$ppc 	= popupImage(640,352,'ppc',$fName);
				$thumb 	= $finalName;
			}				
			
			$content = '';
			$dvbCodes = '';
			$total =  1;
			$episodeNum = (array_key_exists('episode_number', $p) ? $p->episode_number : 0);
			$seasonNum = (array_key_exists('series_number', $p) ? $p->series_number : 0);
			$episodeTitle = (array_key_exists('episode_title', $p) ? $p->episode_title : '');
			
			$display_title 	= html_entity_decode($p->title, ENT_QUOTES | ENT_HTML5);
			$title 			= html_entity_decode($p->title, ENT_QUOTES | ENT_HTML5);
			$programmeId 	= $p->prog_id;
			$episodeId 		= $p->showing_id;
			$shortInfo 		= $p->synopsis[0];
			$info 			= $p->synopsis[1];
			$genre 			= '';
			$bcastId 		= $p->showing_id;
			$sUnix			= strtotime($p->date.' '.$p->time);
			$duration 		= $p->duration;
			$eUnix 			= strtotime('+ '.$duration.' minutes', $sUnix);
			
			$start			= date('Y-m-d H:i:s', $sUnix);
			$end			= date('Y-m-d H:i:s', $eUnix);
			$play_url 		= '';
			
			$sql = "INSERT INTO `app_epg` (`epg_broadcast_id`,`epg_display_title`,`epg_end`,`epg_start`,`epg_episode_id`,`epg_info`,`epg_short_info`,`epg_channel_id`,`epg_episode_title`,`epg_genre`,`epg_programme_id`,`epg_thumb`,`epg_content`,`epg_dvb_codes`,`epg_episode_number`, `epg_series_number`,`epg_total`,`epg_play_url`,`epg_duration`) VALUES (:bcast_id,:display_title,:end,:start,:episode_id,:info,:short_info,:channel_id,:episode_title,:genre,:programme_id,:thumb,:content,:dvbCodes,:episode_num,:season_num,:total,:play_url,:duration)";	
			try {
				$db = getConnection();
				$stmt = $db->prepare($sql);
				$stmt->bindParam(":bcast_id", $bcastId);
				$stmt->bindParam(":display_title", $display_title);
				$stmt->bindParam(":end", $end);
				$stmt->bindParam(":start", $start);
				$stmt->bindParam(":info", $info);
				$stmt->bindParam(":short_info", $shortInfo);
				$stmt->bindParam(":channel_id", $cId);
				$stmt->bindParam(":genre", $genre);
				$stmt->bindParam(":programme_id", $programmeId);
				$stmt->bindParam(":episode_id", $episodeId);
				$stmt->bindParam(":thumb", $thumb);
				$stmt->bindParam(":duration", $duration);
				$stmt->bindParam(":content", $content);
				$stmt->bindParam(":dvbCodes", $dvbCodes);
				$stmt->bindParam(":total", $total);
				$stmt->bindParam(":episode_num", $episodeNum);
				$stmt->bindParam(":season_num", $seasonNum);
				$stmt->bindParam(":episode_title", $episodeTitle);
				$stmt->bindParam(":play_url", $play_url);
				$stmt->execute();
				$db = null;
				echo 'Record added - '.$num.' - image: '.$p->pictures->pictureUsage.PHP_EOL;
			} catch(PDOException $e) {
				echo $e->getMessage().PHP_EOL;		
			}			
		};
	};
	
	function rand_string( $length ) {
		$chars = "abcdefghijklmnopqrstuvwxyz0123456789";	
		$str = '';
		
		$size = strlen( $chars );
		for( $i = 0; $i < $length; $i++ ) {
			$str .= $chars[ rand( 0, $size - 1 ) ];
		}
	
		return $str;
	}
	
	// PDO Function
	function getConnection() {
		$dbhost 		= "db.2ndscreen.tv";//NEW
		$dbuser 		= "SecondScreen";
		$dbpass 		= "9pEPstpHdfe9WTy";
		$dbname		= "SecondScreen";
		$options 		= array(
		    PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
		); 		
	    $dbh = new PDO("mysql:host=$dbhost;dbname=$dbname", $dbuser, $dbpass, $options);
	    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	    return $dbh;			
	}
?>			
						