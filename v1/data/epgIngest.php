<?php
	if(!defined('APPLICATION_PATH')) define('APPLICATION_PATH',  dirname(__FILE__));

	date_default_timezone_set('Europe/London');
	
	$Date = date('Y-m-d');
	$nextIngest = date('Y-m-d', strtotime($Date. ' + 9 days'));
	
	$channelList = "1395,1414,1432,1433,1455,1456,1436,1540,1966,1558,1559,1560,1561,1562,1563,1472,1463,1465,1502,1503,1573,1574,2020,1385,1515,1516,1517,1821,1518,1933,1520,1934,1622,2096,1624,1566,1569,1567,1568,1800,2085,1626,1628,1663,1652";
	
	//Include the GD Resize Function
	include('imgToolsEPG.php');
	
	$call = "http://api.vizimo.com/v1/fullListings?channels=".$channelList."&dates=".$nextIngest;
	
	$username = 'cdm';
	$password = 'v1z1m0';
	
	$curl_session = curl_init($call);
	
	$curl_options = array(
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_HTTPHEADER => array(
			'Accept: application/json',
			'Content-Type: application/json'
		),
		CURLOPT_USERPWD => $username.':'.$password
	);
	
	curl_setopt_array($curl_session, $curl_options);
	$curl_response = curl_exec($curl_session);
	$data = json_decode($curl_response);
	$i = 0;
	foreach($data as $obj) {	
		$channelId = $obj->channel_id;
		//Dates Array Loop
		foreach($obj->dates as $dt) {
			//EPG Line Loop
			foreach($dt->broadcasts as $bc) {
				$thumb = '';
				if(array_key_exists('thumb', $bc)) {
				
					$url = 'http://thumbs.vizimo.com/tv/'.$bc->thumb.'-1024x576.jpg';
					
					$extension 	= 'jpg';
					$fName 		= rand_string(24);
					$finalName 	= $fName.'.'.$extension;
					$img 		= APPLICATION_PATH.'/tmp/'.$finalName;
					
					if(get_http_response_code($url) != "404"){
					   file_put_contents($img, file_get_contents($url));
					}else{
					   file_put_contents($img, file_get_contents($urlAlt));
					}
					
					//Convert & Save Images
					$gcr = gridBox(452,280,'gcr',$fName,100);
					$gc = gridBox(226,140,'gc',$fName,50);
					$tcr = popupImage(200,140,'tcr',$fName);
					$ppa = popupImage(690,352,'ppa',$fName);
					$ppb = popupImage(345,176,'ppb',$fName);
					$ppc = popupImage(640,352,'ppc',$fName);
						
					$thumb = $finalName;
				}
				
				$content = (array_key_exists('content', $bc) ? $bc->content : '');
				$dvbCodes = (array_key_exists('dvb_codes', $bc) ? $bc->dvb_codes : '');
				$total = (array_key_exists('total', $bc) ? $bc->total : 1);
				$episodeNum = (array_key_exists('episode_number', $bc) ? $bc->episode_number : 0);
				$seasonNum = (array_key_exists('season_number', $bc) ? $bc->season_number : 0);
				$episodeTitle = (array_key_exists('episode_title', $bc) ? $bc->episode_title : '');
				
				$display_title 	= html_entity_decode($bc->display_title, ENT_QUOTES | ENT_HTML5);
				$title 			= html_entity_decode($bc->title, ENT_QUOTES | ENT_HTML5);
				$programmeId 	= $bc->programme_id;
				$episodeId 		= $bc->episode_id;
				$start 			= $bc->start;
				$end 			= $bc->end;
				$shortInfo 		= html_entity_decode($bc->short_info, ENT_QUOTES | ENT_HTML5);
				$info 			= html_entity_decode($bc->info, ENT_QUOTES | ENT_HTML5);
				$genre 			= $bc->genre;
				$bcastId 		= $bc->broadcast_id;
				
				$duration		= round(abs(strtotime($end) - strtotime($start)) / 60,2);
					
				$bc_URL = "http://api.vizimo.com/v1/broadcast_info?broadcast_id=$bcastId";
				$bc_curl = curl_init($bc_URL);
				$bc_curl_options = array(
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_HTTPHEADER => array(
						'Accept: application/json',
						'Content-Type: application/json'
					),
					CURLOPT_USERPWD => $username.':'.$password
				);
				curl_setopt_array($bc_curl, $bc_curl_options);
				$bc_curl_response = curl_exec($bc_curl);
				
				$bcData = json_decode($bc_curl_response);
				
				$play_url = (array_key_exists('play_url', $bcData) ? $bcData->play_url : '');
				
				$sql = "INSERT INTO `app_epg` (`epg_broadcast_id`,`epg_display_title`,`epg_end`,`epg_start`,`epg_episode_id`,`epg_info`,`epg_short_info`,`epg_channel_id`,`epg_episode_title`,`epg_genre`,`epg_programme_id`,`epg_thumb`,`epg_content`,`epg_dvb_codes`,`epg_episode_number`, `epg_series_number`,`epg_total`,`epg_play_url`,`epg_duration`) VALUES (:bcast_id,:display_title,:end,:start,:episode_id,:info,:short_info,:channel_id,:episode_title,:genre,:programme_id,:thumb,:content,:dvbCodes,:episode_num,:season_num,:total,:play_url,:duration)";	
				try {
					$db = getConnection();
					$stmt = $db->prepare($sql);
					$stmt->bindParam(":bcast_id", $bcastId);
					$stmt->bindParam(":display_title", $display_title);
					$stmt->bindParam(":end", $end);
					$stmt->bindParam(":start", $start);
					$stmt->bindParam(":info", $info);
					$stmt->bindParam(":short_info", $shortInfo);
					$stmt->bindParam(":channel_id", $channelId);
					$stmt->bindParam(":genre", $genre);
					$stmt->bindParam(":programme_id", $programmeId);
					$stmt->bindParam(":episode_id", $episodeId);
					$stmt->bindParam(":thumb", $thumb);
					$stmt->bindParam(":duration", $duration);
					//Optional Data, Will instert Blank String or default INT if no Content
					$stmt->bindParam(":content", $content);
					$stmt->bindParam(":dvbCodes", $dvbCodes);
					$stmt->bindParam(":total", $total);
					$stmt->bindParam(":episode_num", $episodeNum);
					$stmt->bindParam(":season_num", $seasonNum);
					$stmt->bindParam(":episode_title", $episodeTitle);
					$stmt->bindParam(":play_url", $play_url);
					$stmt->execute();
					$i++;
					$db = null;
					echo 'Record added - '.$i.PHP_EOL;
				} catch(PDOException $e) {
					echo $e->getMessage().PHP_EOL;		
				}		
			}
		}
	}
	
	// close session
	curl_close($curl_session);
	
	//Email here…
	sendEmail($i);
	
	function rand_string( $length ) {
		$chars = "abcdefghijklmnopqrstuvwxyz0123456789";	
		$str = '';
		
		$size = strlen( $chars );
		for( $i = 0; $i < $length; $i++ ) {
			$str .= $chars[ rand( 0, $size - 1 ) ];
		}
	
		return $str;
	}
	
	function get_http_response_code($url) {
	    $headers = get_headers($url);
	    return substr($headers[0], 9, 3);
	}
		
	function sendEmail($i) {
		$date = date('d-m-Y');
		
		$to = 'justin@createdm.com';
		$subject = 'Server Status Update - EPG Ingest';
		
		$headers = "From: server@2ndscreen.tv\r\n";
		$headers .= "Reply-To: no-reply@2ndscreen.tv\r\n"; 
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
		$message = '<html><head><meta content="telephone=no" name="format-detection"><meta content="date=no" name="format-detection"></head><body style="font-family: Helvetica">';
		$message .= '<div style="width: 60%; margin: 0 auto; text-align: center;">';
		$message .= '<img src="http://api.2ndscreen.tv/v1/data/2S.png" style="float-left; width: 44px"><h3>Server Status Update - EPG Ingest</h3>';
		$message .= '<p>The server successfully ingested '.$i.' EPG records</p>';
		$message .= '<hr>';
		$message .= '<p style="color: #999; font-size: 10px">'.$date.'</p>';
		$message .= "</body></html>";
		
		mail($to, $subject, $message, $headers);		
	}
	
	// PDO Function
	function getConnection() {
		$dbhost 		= "db.2ndscreen.tv";//NEW
		$dbuser 		= "SecondScreen";
		$dbpass 		= "9pEPstpHdfe9WTy";
		$dbname		= "SecondScreen";
		$options 		= array(
		    PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
		); 		
	    $dbh = new PDO("mysql:host=$dbhost;dbname=$dbname", $dbuser, $dbpass, $options);
	    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	    return $dbh;			
	}

?>
						