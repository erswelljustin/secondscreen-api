<?php
	//TODO:: PARSE Image Path (url)
	//TODO:: Target Width & Height
	//TODO:: Panel Height
	//TODO:: Save Image Name

	$url = 'http://thumbs.vizimo.com/tv/webANXshameslesss1-1024x576.jpg';
	$file = '/var/www/v1/data/tmp/test123.jpg';
	file_put_contents($file, file_get_contents($url));

	$im = new Imagick($file);
	
	$imageprops = $im->getImageGeometry();
	$width = $imageprops['width'];
	$height = $imageprops['height'];
	if($width > $height){
	    $newHeight = 280;
	    $newWidth = (452 / $height) * $width;
	}else{
	    $newWidth = 452;
	    $newHeight = (280 / $width) * $height;
	}
	$im->resizeImage($newWidth,$newHeight, imagick::FILTER_LANCZOS, 0.9, true);
	$im->cropImage (452,380,0,0);	
	$reflection = $im->clone();
	$reflection->flipImage();
	$gradient = new Imagick();
	$gradient->newPseudoImage($reflection->getImageWidth() + 10, $reflection->getImageHeight() + 10, "canvas:rgba(255,255,255,0.78)");
	$reflection->compositeImage($gradient, imagick::COMPOSITE_OVER, 0, 0);
	$reflection->setImageOpacity( 0.8 );
	$reflection->blurImage(40,30);
	$canvas = new Imagick();
	
	$refH = 70;
	$width = $im->getImageWidth();
	$height = ($im->getImageHeight() + $refH) + 30;
	$canvas->newImage($width, $height, new ImagickPixel("white"));
	$canvas->setImageFormat("jpg");
	$canvas->compositeImage($im, imagick::COMPOSITE_OVER, 0, 0);
	$canvas->compositeImage($reflection, imagick::COMPOSITE_OVER, 0, $im->getImageHeight() + 0);
	$canvas->writeImage('/var/www/v1/data/tmp/fd7e02c8b233fd4e33a0ca7ff97b73ed.jpg');
	$canvas->destroy();
?>