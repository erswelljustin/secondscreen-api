<?php
class HttpBasicAuth extends \Slim\Middleware
{
    protected $realm;
 
    public function __construct($realm = 'Protected Area')
    {
        $this->realm = $realm;
    }
 
    public function deny_access() {
        $res = $this->app->response();
        $res->status(401);
        $res->header('WWW-Authenticate', sprintf('Basic realm="%s"', $this->realm));        
    }

    public function authenticate($username, $password) {        
        if(isset($username) && isset($password)) {
			$sql = "SELECT `user_email` FROM `app_user` WHERE `user_email`='$username' AND `user_password`='$password' AND `user_deleted`=0";
			try{
		        $db = getConnection();
		        $stmt = $db->prepare($sql);
		        $stmt->execute();
		        $valid = $stmt->rowCount();
	            if($valid==1){
	            	$now = microtime(true);
	            	$authUser = $db->query("SELECT `user_user_id` FROM `app_user` WHERE `user_email`='$username' AND `user_password`='$password' AND `user_deleted`=0")->fetchColumn();
	            	$userDate = $db->exec("UPDATE `app_user` SET `user_last_login`='$now' WHERE `user_user_id`='$authUser'");
		        	return true;    
	            } else {
		            return false;
	            }
	            $db = null;
			} catch(PDOException $e) {
				echo '{"error":{"text":'.$e->getMessage().'}}';
				$db = null;
				return false;
			}
                       
        }
        else
            return false;
    }

    public function call()
    {
        $req = $this->app->request();
        $res = $this->app->response();
        $authUser = $req->headers('PHP_AUTH_USER');
        $authPass = $req->headers('PHP_AUTH_PW');
         
        if ($this->authenticate($authUser, $authPass)) {
            $this->next->call();
        } else {
            $this->deny_access();
        }
    }
}