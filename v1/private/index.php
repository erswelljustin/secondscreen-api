<?php
	// Private API for Second Screen v1.0
	//
	// Author: Create Digital Media Ltd :: Justin Erswell
	// Date: 24th January 2014 
	
	require 'Slim/Slim.php';
	require 'Slim/Middleware.php';
	require 'Slim/Middleware/HttpBasicAuth.php';
	
	date_default_timezone_set('Europe/London');
	\Slim\Slim::registerAutoloader();
	$app = new \Slim\Slim();
	$app->add(new \HttpBasicAuth());
	$app->contentType('application/json; charset=utf8mb4'); 
	
	// GET Routes
	$app->get('/loginUser', 'loginUser');
	$app->get('/getUserInfo', 'getUserInfo');
	$app->get('/getEpisodes/:offset/:limit/:genres', 'getEpisodes');
	$app->get('/getEpisodesByProgrammeId/:pid','getEpisodesByProgrammeId');
	$app->get('/getWatchList', 'getWatchList');
	$app->get('/getWatchHistory', 'getWatchHistory');
	$app->get('/getGenres', 'getGenres');
	$app->get('/getEpisodesByGenre/:gid', 'getEpisodesByGenre');
	$app->get('/searchEpisodes/:keyword/:limit', 'searchEpisodes');
	$app->get('/searchSeries/:keyword/:limit', 'searchSeries');
	$app->get('/getBroadcasters', 'getBroadcasters');
	$app->get('/getTrending/:genres', 'getTrending');
	$app->get('/getComingSoon', 'getComingSoon');
	$app->get('/getComingSoonById/:id', 'getComingSoonById');
	$app->get('/getEPG/:date/:genres', 'getEPG');
	$app->get('/getEPGById/:eid', 'getEPGById');
	$app->get('/getMetaInfo', 'getMetaInfo');
	$app->get('/getEPGReminders', 'getEPGReminders');
	$app->get('/scheduleParsePush/:entityId/:userId/:type', 'scheduleParsePush');
	$app->get('/thisIsATest/:name', 'thisIsATest');
	// TEMP
	$app->get('/getLatestUnwatchedEpisodeInProgramme/:pid', 'getLatestUnwatchedEpisodeInProgramme');
	// POST Rotes
	$app->post('/setDeviceToken/', 'setDeviceToken');
	$app->post('/setEPGReminder/', 'setEPGReminder');
	$app->post('/avatarUpload/', 'avatarUpload');
	$app->post('/updateUserInfo/', 'updateUserInfo');
	$app->post('/addToWatchList/', 'addToWatchList');
	$app->post('/setWatchStatus/', 'setWatchStatus');
	$app->post('/setWatchListIndex/', 'setWatchListIndex');
	$app->post('/setAdditionalChannels/', 'setAdditionalChannels');
	
	// Private Functions
	function authUserId(){
		$authUser = $_SERVER['PHP_AUTH_USER'];
		$sql = "SELECT `user_user_id` FROM `app_user` WHERE `user_email`='$authUser'";
		try{
	        $db = getConnection();
	        $stmt = $db->prepare($sql);
	        $stmt->execute();
	        $user = $stmt->fetchColumn();
	        $db = null;
			return $user;
		} catch(PDOException $e) {
			echo '{"error":{"text":'.$e->getMessage().'}}';
			$db = null;
		}
	}
	function setNotifcation($userId, $type, $entityId, $status, $date) {

		$now = microtime(true);
		$sql = "INSERT INTO `app_notification` (`noti_user_id`,`noti_type`,`noti_entity_id`,`noti_status`,`noti_run_date`,`noti_updated`) VALUES (:noti_user_id,:noti_type,:noti_entity_id,:noti_status,:noti_run_date,'$now') ON DUPLICATE KEY UPDATE `noti_status`=:noti_status, `noti_updated`='$now'";
		try {
			$db = getConnection();
			$stmt = $db->prepare($sql);
			$stmt->bindParam(":noti_user_id", $userId);
			$stmt->bindParam(":noti_type", $type);
			$stmt->bindParam(":noti_entity_id", $entityId);
			$stmt->bindParam(":noti_status", $status);
			$stmt->bindParam(":noti_run_date", $date);
			$stmt->execute();
			$db = null;
		} catch(PDOException $e) {
	        echo '{"error":';
				echo '{"text":'. $e->getMessage() .'}';
				echo '{"SQL":'.$sql.'}';
			echo '}';		
		}			
	}	
	function scheduleParsePush($entityId, $userId, $type) {
		$APPLICATION_ID = "gv8tyCWaBfuf6TOhbKfmo7ZVzDJ6n3Vt3Wc9vJPh";
		$REST_API_KEY = "sGNMFvvkrNbd4X7lGEOylrwzsMgAOQqU8tg0D4JY";
		$db = getConnection();
		$userDevices = $db->query("SELECT `devi_device_token` FROM `app_device` WHERE `devi_user_id`='$userId'")->fetchAll(PDO::FETCH_OBJ);
		
		$devArr = array();
		foreach ($userDevices as $result) {
		  array_push($devArr, $result->devi_device_token);
		}
		
		switch ($type){
			case 0;
				$epgShow = $db->query("SELECT `epg_start`, `epg_display_title` FROM `app_epg` WHERE `epg_epg_id`='$entityId'")->fetch(PDO::FETCH_OBJ);
				$alert = $epgShow->epg_display_title." is about to start (".date('h:i', strtotime($epgShow->epg_start)).")";
				$channel = 'epg_reminder';
				$schedule = date("Y-m-d\TH:i:s",strtotime("-10 minutes",strtotime($epgShow->epg_start)));
				break;
			case 1;
				$vodShow = $db->query("SELECT `epis_expiry`, `epis_title` FROM `meta_episodes` WHERE `epis_episode_id`='$entityId'")->fetch(PDO::FETCH_OBJ);
				$alert = $vodShow->epis_title." is about to expire (".date('d/m/Y h:i', strtotime($vodShow->epis_expiry)).")";
				$channel = 'vod_expiry';
				break;
		}
		
		$db = null;
		
		$url = 'https://api.parse.com/1/push';
		    $data = array(
			"where" => array(
				"deviceType" => "ios",
				"channels" => array(
					'$in' => array(
						$channel
					)
				),
				"deviceToken" => array(
					'$in' => $devArr
				)
			),
			"push_time" => $schedule,
		            'data' => array(
		                'alert' => $alert,
				'action' => array(
					'entity_id' => $entityId,
					'alert_type' => $type
				),
		            ),
		        );
		        $_data = json_encode($data);
		        $headers = array(
		            'X-Parse-Application-Id: ' . $APPLICATION_ID,
		            'X-Parse-REST-API-Key: ' . $REST_API_KEY,
		            'Content-Type: application/json',
		            'Content-Length: ' . strlen($_data),
		        );
		
		        $curl = curl_init($url);
		        curl_setopt($curl, CURLOPT_POST, 1);
		        curl_setopt($curl, CURLOPT_POSTFIELDS, $_data);
		        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
		        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		        $response = curl_exec($curl);		
				echo $response;
	}
	// Get Methods
	function loginUser() {
		$now = microtime(true);
		$authUser = authUserId();
		$sql = "SELECT `user_account_type`, `user_avatar_url`, `user_email`, `user_first_name`, `user_last_name`, `user_gender`, `user_user_id`, `user_first_registered`, `user_additional_channels` FROM `app_user` WHERE `user_user_id`='$authUser'";
		try{
	        $db = getConnection();
	        $stmt = $db->prepare($sql);
	        $stmt->execute();
	        $user = $stmt->fetch(PDO::FETCH_OBJ);
	        //Update Last Login Date
	        $userDate = $db->exec("UPDATE `app_user` SET `user_last_login`='$now' WHERE `user_user_id`='$authUser'");	
	        //End
	        $db = null;
	        echo json_encode($user);
		} catch(PDOException $e) {
			echo '{"error -- epsiodes":{"text":'.$e->getMessage().'}}';
			$db = null;
		} 	
	}
	function getUserInfo() {
		$authUser = authUserId();
		$sql = "SELECT `user_account_type`, `user_avatar_url`, `user_email`, `user_first_name`, `user_last_name`, `user_gender`, `user_user_id`, `user_first_registered`, `user_additional_channels` FROM `app_user` WHERE `user_user_id`='$authUser'";
		try{
	        $db = getConnection();
	        $stmt = $db->prepare($sql);
	        $stmt->execute();
	        $user = $stmt->fetch(PDO::FETCH_OBJ);
	        $db = null;
	        echo json_encode($user);
		} catch(PDOException $e) {
			echo '{"error -- epsiodes":{"text":'.$e->getMessage().'}}';
			$db = null;
		} 		
	}
	function getEpisodes($offset, $limit, $genres) {
		$channels = '1395,1414,1432,1433,1455,1456,1436,1540,1966,1558,1559,1560,1561,1562,1563,1472,1463,1465,1502,1503,1573,1574,2020,1385,1515,1516,1517,1821,1518,1933,1520,1934';
		
		$authUser = authUserId();
		$db = getConnection();
		
		$userChannels = $db->query("SELECT `user_additional_channels` FROM `app_user` WHERE `user_user_id`='$authUser'")->fetchColumn();
		
		if(!$userChannels==''){
			$channelIds = $channels.',';
			$channelIds .= $userChannels;
		} else {
			$channelIds = $channels;
		}
		$chanArray = explode(',', $channelIds);
		
		$str = "";
		foreach($chanArray as $c){
			$res = $db->query("SELECT `chan_title` FROM `meta_channel` WHERE `chan_vizimo_code`=$c")->fetchColumn();
			$str .= $res.',';
		}
		
		$chanList = rtrim($str,",");
		$genreList = rtrim($genres, ",");

		$db = null;
		$offset = intval($offset); 
		$limit = intval($limit);
		
		if($genreList!='*') {
			$sql = "SELECT * FROM `vMeta_Episodes` WHERE `epis_image_url`<>'' AND find_in_set(`epis_channel`,:channels)>0 AND find_in_set(`epis_genre`,'$genreList')>0 GROUP BY `epis_programme_id` ORDER BY `epis_broadcast_date` DESC LIMIT :offset,:limit";	
		} else {
			$sql = "SELECT * FROM `vMeta_Episodes` WHERE `epis_image_url`<>'' AND find_in_set(`epis_channel`,:channels)>0   GROUP BY `epis_programme_id` ORDER BY `epis_broadcast_date` DESC LIMIT :offset,:limit";	
		}
		try{
			$db = getConnection();
			$db->exec("SET NAMES 'utf8mb4' COLLATE 'utf8mb4_unicode_ci';");
			$stmt = $db->prepare($sql);
			$stmt->bindParam(":offset", $offset, PDO::PARAM_INT);	        
		    $stmt->bindParam(":limit", $limit, PDO::PARAM_INT);
			$stmt->bindParam(":channels", $chanList);
		    $stmt->execute();
		    $episodes = $stmt->fetchAll(PDO::FETCH_OBJ);
		    $db = null;
		    echo json_encode($episodes);
		} catch(PDOException $e) {
			echo '{"error -- epsiodes":{"text":'.$e->getMessage().'}}';
			$db = null;
		} 
	}
	function getLatestUnwatchedEpisodeInProgramme($pid) {
		$authUser = authUserId();
		$db = getConnection();
		$episodes = $db->query("SELECT * FROM `vMeta_Episodes` WHERE `epis_programme_id`='$pid' ORDER BY `epis_broadcast_date` DESC")->fetchAll(PDO::FETCH_OBJ);
		
		foreach($episodes as $e) {
			$eid = $e->epis_episode_id;
			$wast = $db->query("SELECT COUNT(`wast_watchstatus_id`) AS `wast_number` FROM `app_watch_status` WHERE `wast_episode_id`='$eid' AND `wast_user_id`='$authUser' AND `wast_status`=1")->fetchColumn();
			if($wast == 0) {
				//echo $e->epis_title;
				return $e;
			}
		}
	}
	function getLatestUnwatchedEpisode($eid) {
		$sql = "SELECT e.*, IFNULL(s.`wast_status`,0) AS `epis_watched_status`FROM `vMeta_Episodes` e LEFT JOIN `app_watch_status` s ON s.`wast_episode_id` = e.`epis_episode_id` WHERE e.`epis_episode_id`=:eid HAVING `epis_watched_status`<1 ORDER BY e.`epis_broadcast_date` DESC LIMIT 1";	
		try{
	        $db = getConnection();
	        $stmt = $db->prepare($sql);
	        $stmt->bindParam(':eid', $eid);
	        $stmt->execute();
	        $episode = $stmt->fetch(PDO::FETCH_OBJ);
	        $db = null;
	        return $episode;
		} catch(PDOException $e) {
			echo '{"error -- epsiodes":{"text":'.$e->getMessage().'}}';
			$db = null;
		} 
	}	
	function getEpisodesByProgrammeId($pid) {
		$authUser = authUserId();
		$sql = "SELECT e.*,  IFNULL(wali_status, 0) AS epis_watchlist_status, IFNULL(wast_status,0) AS epis_watched_status FROM vMeta_Episodes e LEFT JOIN app_watch_list wl ON e.`epis_episode_id` = wl.wali_episode_id AND wl.`wali_user_id`='$authUser' LEFT JOIN app_watch_status ws ON e.`epis_episode_id` = ws.wast_episode_id AND ws.`wast_user_id`='$authUser' WHERE e.`epis_programme_id`=:pid ORDER BY `epis_broadcast_date` DESC";
		try{
	        $db = getConnection();
			$db->exec("SET NAMES 'utf8mb4' COLLATE 'utf8mb4_unicode_ci';");
	        $stmt = $db->prepare($sql);
	        $stmt->bindParam(':pid', $pid);
	        $stmt->execute();
	        $episodes = $stmt->fetchAll(PDO::FETCH_OBJ);
	        $seriesLink = $db->query("SELECT COUNT(`wali_is_series`) AS series_link FROM `app_watch_list` WHERE `wali_programme_id`='$pid' AND `wali_user_id`='$authUser' AND `wali_is_series`='1' AND `wali_status`='1' ORDER BY `wali_is_series` DESC LIMIT 1")->fetch(PDO::FETCH_OBJ);
	        $db = null;           
			$seriesNumbers = array();
			
			// Loop through returned episodes
			foreach($episodes as $episode) {
			
				// Get the series number for the episode
				$seriesNumber = $episode->epis_series;
				
				// Is the series number already in our series array
				if (in_array($seriesNumber, $seriesNumbers) == false) {
					// If the series number is NOT in the array then add it
					array_push($seriesNumbers, $seriesNumber);
				}
			}
			
			// Sort array based on series number
			asort($seriesNumbers);
			
			// New array to store the 'grouped by series' episodesx§
			$allSeries = array();
			
			// Loop through the available series numbers
			foreach($seriesNumbers as $seriesNumber) {
				
				// New array to store episodes for this series
				$seriesEpisodes = array();
				
				// Create our series object to store the series data
				$series = new stdClass();
				
				// Assign the series number
				$series->number = $seriesNumber;
				
				// Loop through each episode
				foreach($episodes as $episode) {
					// If the episode series number is the same as the current series number then add the episode
					// to our array
					if($episode->epis_series == $seriesNumber) {
						array_push($seriesEpisodes, $episode);
					}
				}
				
				// Assign the series episodes
				$series->episodes = $seriesEpisodes;
				
				// Add the episode series array to our 'grouped by series' array
				array_push($allSeries, $series);
				
				// TODO :
				// Ideally we should probably find a way to filter the array dynamically rather than enumberating through it every time
				// but I can't be bothered....
				
				//$episodesInSeries = array_filter($episodes, function($episode) {
				//	return $episode->epis_series == '1'; 
				//});
			}
			
			$programme = $seriesLink;
			$programme->episodes = $allSeries;
				
			echo(json_encode($programme));     
		} catch(PDOException $e) {
			echo '{"error -- epsiodes":{"text":'.$e->getMessage().'}}';
			$db = null;
		} 
		
	}
	function getEpisodesById($eid) {
		$sql = "SELECT `epis_episode_id`, `epis_genre`, `epis_title`, `epis_channel`, `epis_series_title`, `epis_series`, `epis_broadcast_date`, `epis_number`, `epis_expiry`, `epis_synopsis`, `epis_synopsis_long`, `epis_image_url`, `epis_vod_id`, `epis_web_url`, `epis_inapp_url`, `epis_status`, `epis_atlas_id`, `epis_programme_id` FROM `meta_episodes` WHERE `epis_episode_id` =:eid";
		try{
	        $db = getConnection();
	        $stmt = $db->prepare($sql);
	        $stmt->bindParam(':eid', $eid);
	        $stmt->execute();
	        $episode = $stmt->fetch(PDO::FETCH_OBJ);
	        $db = null;
	        return $episode;
		} catch(PDOException $e) {
			echo '{"error -- epsiodes":{"text":'.$e->getMessage().'}}';
			$db = null;
		} 		
	}
	function getWatchList() {
		$authUser = authUserId();
		$sql = "SELECT `wali_watchlist_id`, `wali_user_id`, `wali_episode_id`, `wali_programme_id`, `wali_status`, `wali_is_series`, `wali_index` FROM `app_watch_list` WHERE `wali_user_id`='$authUser' AND `wali_status`=1 ORDER BY `wali_updated` DESC";
		try{
	        $db = getConnection();
	        $stmt = $db->prepare($sql);
	        $stmt->execute();
	        $waliObj = $stmt->fetchAll(PDO::FETCH_OBJ); 
			$db = null;
			
			$episodes = array();
			
	        foreach($waliObj as $obj) {
		        $seriesFlag = $obj->wali_is_series;
		        $seriesId = $obj->wali_programme_id;
		        $episodeId = $obj->wali_episode_id;
		        $watchListId = $obj->wali_watchlist_id;
		        $watchListIndex = $obj->wali_index;
				
				$episode = getLatestUnwatchedEpisodeInProgramme($seriesId);
				
				if(!$episode == NULL) {
					$episode = (array)$episode;
					$episode['wali_watchlist_id'] = $watchListId;
					$episode['wali_index'] = $watchListIndex;
					$episode = (object)$episode;
					array_push($episodes, $episode);	
				}
	        }
	        
	        echo json_encode($episodes);

		} catch(PDOException $e) {
			echo '{"error -- epsiodes":{"text":'.$e->getMessage().'}}';
			$db = null;
		} 			
	}
	function getWatchHistory() {
		$authUser = authUserId();
		$sql = "SELECT `wast_watchstatus_id`, `wast_date_watched`, `wast_episode_id`, `wast_status`, `wast_user_id` FROM `app_watch_status` WHERE `wast_user_id`='$authUser' AND `wast_status`='1' ORDER BY `wast_date_watched` DESC";
		try{
	        $db = getConnection();
	        $stmt = $db->prepare($sql);
	        $stmt->execute();
	        $wastObj = $stmt->fetchAll(PDO::FETCH_OBJ); 
			$db = null;
			
			$episodes = array();
			
	        foreach($wastObj as $obj) {
		        $episodeId = $obj->wast_episode_id;
				
				$episode = getEpisodesById($episodeId);
				
				if(!$episode == NULL) {
					array_push($episodes, $episode);	
				}
	        }
	        
	        echo json_encode($episodes);

		} catch(PDOException $e) {
			echo '{"error -- epsiodes":{"text":'.$e->getMessage().'}}';
			$db = null;
		} 			
	}
	function getMetaInfo() {
		$authUser = authUserId();
		$db = getConnection();
		$waliCount = $db->query("SELECT COUNT(`wali_watchlist_id`) AS `wali_count` FROM `app_watch_list` WHERE `wali_user_id`='$authUser' AND `wali_status`=1")->fetchColumn();
		$wastCount = $db->query("SELECT COUNT(`wast_watchstatus_id`) AS `wast_count` FROM `app_watch_status` WHERE `wast_user_id`='$authUser' AND `wast_status`=1")->fetchColumn();
		echo '{"wali_count":'.json_encode($waliCount).',"wast_count":'.json_encode($wastCount).'}';
		$db = null;
	}
	function getBroadcasters() {
		$sql = 'SELECT * FROM `meta_channel`';	
		try{
	        $db = getConnection();
	        $stmt = $db->prepare($sql);
	        $stmt->execute();
	        $channels = $stmt->fetchAll(PDO::FETCH_OBJ);
	        $db = null;
	        echo json_encode($channels);
		} catch(PDOException $e) {
			echo '{"error -- epsiodes":{"text":'.$e->getMessage().'}}';
			$db = null;
		} 		
	}
	function getTrending($genres) {

        $genreList = rtrim($genres, ',');

        if($genreList!='*') {
            $sql = "SELECT e.*, t.`tren_count` AS `epis_trending_count` FROM `vMeta_Episodes` e INNER JOIN `app_trending` t ON t.`tren_episode_id` = e.`epis_episode_id` WHERE find_in_set(`epis_genre`,'$genreList')>0 ORDER BY t.`tren_count` DESC";
        } else {
            $sql = "SELECT e.*, t.`tren_count` AS `epis_trending_count` FROM `vMeta_Episodes` e INNER JOIN `app_trending` t ON t.`tren_episode_id` = e.`epis_episode_id` ORDER BY t.`tren_count` DESC";
        }
		try{
	        $db = getConnection();
	        $stmt = $db->prepare($sql);
	        $stmt->execute();
	        $episodes = $stmt->fetchAll(PDO::FETCH_OBJ);
	        $db = null;
	        echo json_encode($episodes);
		} catch(PDOException $e) {
			echo '{"error -- epsiodes":{"text":'.$e->getMessage().'}}';
			$db = null;
		} 		
	}
    function getComingSoon() {
	  	 $sql = "SELECT * FROM `app_coming_soon` ORDER BY `cmso_publish_date` DESC";
      	 try{
            $db = getConnection();
            $stmt = $db->prepare($sql);
            $stmt->execute();
            $cmso = $stmt->fetchAll(PDO::FETCH_OBJ);
            $db = null;
            echo json_encode($cmso);
        	} catch(PDOException $e) {
            echo '{"error -- coming soon":{"text":'.$e->getMessage().'}}';
            $db = null;
       	}
	}
	function getComingSoonById($id) {
		 $sql = "SELECT * FROM `app_coming_soon` WHERE `cmso_coming_soon_id`=:id";
		 try{
		   $db = getConnection();
		   $db->exec("SET NAMES 'utf8mb4' COLLATE 'utf8mb4_unicode_ci';");
		   $stmt = $db->prepare($sql);
		   $stmt->bindParam(':id', $id);
		   $stmt->execute();
		   $cmso = $stmt->fetch(PDO::FETCH_OBJ);
		   $db = null;
		   echo json_encode($cmso);
		} catch(PDOException $e) {
		   echo '{"error -- coming soon":{"text":'.$e->getMessage().'}}';
		   $db = null;
		}		
	}
	function getEPG($date, $genres) {
		$channels = '1395,1414,1432,1433,1455,1456,1436,1540,1966,1558,1559,1560,1561,1562,1563,1472,1463,1465,1502,1503,1573,1574,2020,1385,1515,1516,1517,1821,1518,1933,1520,1934,';
		$authUser = authUserId();
		$db = getConnection();
		$channels .= $db->query("SELECT `user_additional_channels` FROM `app_user` WHERE `user_user_id`='$authUser'")->fetchColumn();
		$db = null;


        $genreList = rtrim($genres, ',');

        if($genreList!='*') {
            $sql = "SELECT `epg_epg_id`, `epg_display_title`, `epg_start`, `epg_end`, `epg_thumb`, `epg_channel_logo`, `epg_channel_id` FROM `vMeta_EPG` WHERE `epg_start` >=:date AND `epg_start` < DATE_ADD(:date, INTERVAL 2 HOUR) AND find_in_set(`epg_channel_id`,:channels)>0 AND find_in_set(`epg_genre`,'$genreList')>0
				ORDER BY
					CASE WHEN `epg_channel_id` = '1395' THEN `epg_end` END DESC, -- BBC 1
					CASE WHEN `epg_channel_id` = '1414' THEN `epg_end` END DESC, -- BBC 2
					CASE WHEN `epg_channel_id` = '1540' THEN `epg_end` END DESC, -- ITV 1
					CASE WHEN `epg_channel_id` = '1463' THEN `epg_end` END DESC, -- Channel 4
					CASE WHEN `epg_channel_id` = '1517' THEN `epg_end` END DESC, -- Channel 5
					CASE WHEN `epg_channel_id` = '1432' THEN `epg_end` END DESC, -- BBC 3
					CASE WHEN `epg_channel_id` = '1433' THEN `epg_end` END DESC, -- BBC 4
					CASE WHEN `epg_channel_id` = '1558' THEN `epg_end` END DESC, -- ITV 2
					CASE WHEN `epg_channel_id` = '1560' THEN `epg_end` END DESC, -- ITV 3
					CASE WHEN `epg_channel_id` = '1562' THEN `epg_end` END DESC, -- ITV 4
					CASE WHEN `epg_channel_id` = '1502' THEN `epg_end` END DESC, -- E4
					CASE WHEN `epg_channel_id` = '1573' THEN `epg_end` END DESC, -- More 4
					CASE WHEN `epg_channel_id` = '2020' THEN `epg_end` END DESC, -- 4 Seven
					CASE WHEN `epg_channel_id` = '1515' THEN `epg_end` END DESC, -- Film 4
					CASE WHEN `epg_channel_id` = '1455' THEN `epg_end` END DESC, -- CBBC
					CASE WHEN `epg_channel_id` = '1456' THEN `epg_end` END DESC, -- CeBeebies
					CASE WHEN `epg_channel_id` = '1472' THEN `epg_end` END DESC, -- CiTV
					CASE WHEN `epg_channel_id` = '1385' THEN `epg_end` END DESC, -- 4 Music
					CASE WHEN `epg_channel_id` = '1518' THEN `epg_end` END DESC, -- 5 Star
					CASE WHEN `epg_channel_id` = '1520' THEN `epg_end` END DESC, -- 5 USA
					CASE WHEN `epg_channel_id` = '1436' THEN `epg_end` END DESC, -- BBC News
					CASE WHEN `epg_channel_id` = '1966' THEN `epg_end` END DESC, -- ITV 1+1
					CASE WHEN `epg_channel_id` = '1559' THEN `epg_end` END DESC, -- ITV 2+1
					CASE WHEN `epg_channel_id` = '1561' THEN `epg_end` END DESC, -- ITV 3+1
					CASE WHEN `epg_channel_id` = '1563' THEN `epg_end` END DESC, -- ITV 4+1
					CASE WHEN `epg_channel_id` = '1465' THEN `epg_end` END DESC, -- Channel 4+1
					CASE WHEN `epg_channel_id` = '1503' THEN `epg_end` END DESC, -- E4+1
					CASE WHEN `epg_channel_id` = '1574' THEN `epg_end` END DESC, -- More 4+1
					CASE WHEN `epg_channel_id` = '1516' THEN `epg_end` END DESC, -- Film 4+1
					CASE WHEN `epg_channel_id` = '1821' THEN `epg_end` END DESC, -- Channel 5+1
					CASE WHEN `epg_channel_id` = '1933' THEN `epg_end` END DESC, -- 5 Star+1
					CASE WHEN `epg_channel_id` = '1934' THEN `epg_end` END DESC  -- 5 USA+1
				";
        } else {
            $sql = "SELECT `epg_epg_id`, `epg_display_title`, `epg_start`, `epg_end`, `epg_thumb`, `epg_channel_logo`, `epg_channel_id` FROM `vMeta_EPG` WHERE `epg_start` >=:date AND `epg_start` < DATE_ADD(:date, INTERVAL 2 HOUR) AND find_in_set(`epg_channel_id`,:channels)>0
				ORDER BY
					CASE WHEN `epg_channel_id` = '1395' THEN `epg_end` END DESC, -- BBC 1
					CASE WHEN `epg_channel_id` = '1414' THEN `epg_end` END DESC, -- BBC 2
					CASE WHEN `epg_channel_id` = '1540' THEN `epg_end` END DESC, -- ITV 1
					CASE WHEN `epg_channel_id` = '1463' THEN `epg_end` END DESC, -- Channel 4
					CASE WHEN `epg_channel_id` = '1517' THEN `epg_end` END DESC, -- Channel 5
					CASE WHEN `epg_channel_id` = '1432' THEN `epg_end` END DESC, -- BBC 3
					CASE WHEN `epg_channel_id` = '1433' THEN `epg_end` END DESC, -- BBC 4
					CASE WHEN `epg_channel_id` = '1558' THEN `epg_end` END DESC, -- ITV 2
					CASE WHEN `epg_channel_id` = '1560' THEN `epg_end` END DESC, -- ITV 3
					CASE WHEN `epg_channel_id` = '1562' THEN `epg_end` END DESC, -- ITV 4
					CASE WHEN `epg_channel_id` = '1502' THEN `epg_end` END DESC, -- E4
					CASE WHEN `epg_channel_id` = '1573' THEN `epg_end` END DESC, -- More 4
					CASE WHEN `epg_channel_id` = '2020' THEN `epg_end` END DESC, -- 4 Seven
					CASE WHEN `epg_channel_id` = '1515' THEN `epg_end` END DESC, -- Film 4
					CASE WHEN `epg_channel_id` = '1455' THEN `epg_end` END DESC, -- CBBC
					CASE WHEN `epg_channel_id` = '1456' THEN `epg_end` END DESC, -- CeBeebies
					CASE WHEN `epg_channel_id` = '1472' THEN `epg_end` END DESC, -- CiTV
					CASE WHEN `epg_channel_id` = '1385' THEN `epg_end` END DESC, -- 4 Music
					CASE WHEN `epg_channel_id` = '1518' THEN `epg_end` END DESC, -- 5 Star
					CASE WHEN `epg_channel_id` = '1520' THEN `epg_end` END DESC, -- 5 USA
					CASE WHEN `epg_channel_id` = '1436' THEN `epg_end` END DESC, -- BBC News
					CASE WHEN `epg_channel_id` = '1966' THEN `epg_end` END DESC, -- ITV 1+1
					CASE WHEN `epg_channel_id` = '1559' THEN `epg_end` END DESC, -- ITV 2+1
					CASE WHEN `epg_channel_id` = '1561' THEN `epg_end` END DESC, -- ITV 3+1
					CASE WHEN `epg_channel_id` = '1563' THEN `epg_end` END DESC, -- ITV 4+1
					CASE WHEN `epg_channel_id` = '1465' THEN `epg_end` END DESC, -- Channel 4+1
					CASE WHEN `epg_channel_id` = '1503' THEN `epg_end` END DESC, -- E4+1
					CASE WHEN `epg_channel_id` = '1574' THEN `epg_end` END DESC, -- More 4+1
					CASE WHEN `epg_channel_id` = '1516' THEN `epg_end` END DESC, -- Film 4+1
					CASE WHEN `epg_channel_id` = '1821' THEN `epg_end` END DESC, -- Channel 5+1
					CASE WHEN `epg_channel_id` = '1933' THEN `epg_end` END DESC, -- 5 Star+1
					CASE WHEN `epg_channel_id` = '1934' THEN `epg_end` END DESC  -- 5 USA+1
				";
        }


    	try{
            $db = getConnection();
            $db->exec("SET NAMES 'utf8mb4' COLLATE 'utf8mb4_unicode_ci';");		
            $stmt = $db->prepare($sql);
            $stmt->bindParam(':date', $date);
			$stmt->bindParam(':channels', $channels);
            $stmt->execute();
            $epg = $stmt->fetchAll(PDO::FETCH_OBJ);
            $db = null;
            echo json_encode($epg);
        } catch(PDOException $e) {
            echo '{"error -- EPG":{"text":'.$e->getMessage().'}}';
            $db = null;
        }	    
	}
	function getEPGById($eid){
		$authUser = authUserId();
    	$sql = "SELECT * FROM `vMeta_EPG` WHERE `epg_epg_id` =:eid";
    		try{
            $db = getConnection();
            $db->exec("SET NAMES 'utf8mb4' COLLATE 'utf8mb4_unicode_ci';");		
            $stmt = $db->prepare($sql);
            $stmt->bindParam(':eid', $eid);
            $stmt->execute();
            $epg = $stmt->fetch(PDO::FETCH_OBJ);
	   	 	$epgRS = $db->query("SELECT `noti_status` FROM `app_notification` WHERE `noti_entity_id`='$eid' AND `noti_user_id`='$authUser' LIMIT 1")->fetchColumn();
            $db = null;
            $epg->epg_reminder_status = (int) $epgRS;
            echo json_encode($epg);
        } catch(PDOException $e) {
            echo '{"error -- EPG":{"text":'.$e->getMessage().'}}';
            $db = null;
        }	    
	}
	function getEPGReminders(){
		$authUser = authUserId();
		$db = getConnection();
    	$epgIds = $db->query("SELECT `noti_entity_id` FROM `app_notification` WHERE `noti_user_id` ='$authUser' AND `noti_status`=1")->fetchAll(PDO::FETCH_OBJ);
		//var_dump($epgIds);
		echo '[';
		$epgArr = '';
		ob_start();
		foreach($epgIds as $obj) {
			$id = $obj->noti_entity_id;
			if(!$id==0) {
				$res = $db->query("SELECT `epg_epg_id`, `epg_display_title`, `epg_start`, `epg_end`, `epg_thumb`, `epg_channel_logo`, `epg_channel_id` FROM `vMeta_EPG` WHERE `epg_epg_id`='$id' ORDER BY `epg_start` DESC")->fetch(PDO::FETCH_OBJ);
				echo json_encode($res).',';
			}
		}
		$output = ob_get_clean();
		$epgArr .= $output;
		echo rtrim($epgArr, ',');
		
		$db = null;
		echo ']';
	}
	// Search Functions inc. Genres
	function getGenres() {
		$sql = 'SELECT `genr_vizimo_id`, `genr_name` FROM `meta_genre`';	
		try{
	        $db = getConnection();
	        $stmt = $db->prepare($sql);
	        $stmt->execute();
	        $genres = $stmt->fetchAll(PDO::FETCH_OBJ);
	        $db = null;
	        echo json_encode($genres);
		} catch(PDOException $e) {
			echo '{"error -- epsiodes":{"text":'.$e->getMessage().'}}';
			$db = null;
		} 		
	}
	function getEpisodesByGenre($gid) {
		$sql = 'SELECT * FROM `vMeta_Episodes` WHERE `epis_image_url`<>"" AND `epis_genre`=:gid GROUP BY `epis_programme_id` ORDER BY `epis_broadcast_date` DESC';	
		try{
	        $db = getConnection();
	        $stmt = $db->prepare($sql);
			$stmt->bindParam(":gid", $gid);
	        $stmt->execute();
	        $episodes = $stmt->fetchAll(PDO::FETCH_OBJ);
	        $db = null;
	        echo json_encode($episodes);
		} catch(PDOException $e) {
			echo '{"error -- epsiodes":{"text":'.$e->getMessage().'}}';
			$db = null;
		} 
	}
	function searchEpisodes($keyword, $limit) {
		$kw = '%'.$keyword.'%';
		$sql = 'SELECT DISTINCT vMeta_Episodes.* FROM `vMeta_Episodes` WHERE `epis_series_title` LIKE :keyword OR `epis_synopsis_long` LIKE :keyword AND `epis_image_url`<>"" ORDER BY `epis_broadcast_date` DESC LIMIT :limit';	
		try{
	        $db = getConnection();
	        $stmt = $db->prepare($sql);
	        $stmt->bindParam(':keyword', $kw);
	        $stmt->bindParam(":limit", intval(trim($limit)), PDO::PARAM_INT);
	        $stmt->execute();
	        $episodes = $stmt->fetchAll(PDO::FETCH_OBJ);
	        $db = null;
	        echo json_encode($episodes);
		} catch(PDOException $e) {
			echo '{"error -- epsiodes":{"text":'.$e->getMessage().'}}';
			$db = null;
		} 		
	}
	function searchSeries($keyword, $limit) {
		$kw = '%'.$keyword.'%';
		$sql = 'SELECT DISTINCT vMeta_Episodes.* FROM `vMeta_Episodes` WHERE `epis_series_title` LIKE :keyword OR `epis_synopsis_long` LIKE :keyword AND `epis_image_url`<>"" GROUP BY `epis_programme_id` ORDER BY `epis_broadcast_date` DESC LIMIT :limit';	
		try{
	        $db = getConnection();
	        $stmt = $db->prepare($sql);
	        $stmt->bindParam(':keyword', $kw);
	        $stmt->bindParam(":limit", intval(trim($limit)), PDO::PARAM_INT);
	        $stmt->execute();
	        $episodes = $stmt->fetchAll(PDO::FETCH_OBJ);
	        $db = null;
	        echo json_encode($episodes);
		} catch(PDOException $e) {
			echo '{"error -- epsiodes":{"text":'.$e->getMessage().'}}';
			$db = null;
		} 		
	}
	// Post Methods
	function setDeviceToken() {
		$now = microtime(true);
		$authUser = authUserId();
		$request = \Slim\Slim::getinstance()->request();
		$body = $request->getBody();
		$item = json_decode($body);
		
		$sql = "INSERT INTO `app_device` (`devi_user_id`, `devi_device_token`, `devi_device_type`, `devi_updated`) VALUES (:devi_user_id,:devi_device_token,:devi_device_type,'$now') ON DUPLICATE KEY UPDATE `devi_updated`='$now'";
		try {
			$db = getConnection();
			$stmt = $db->prepare($sql);
			$stmt->bindParam(':devi_user_id', $authUser);
			$stmt->bindParam(':devi_device_token', $item->devi_device_token);
			$stmt->bindParam(':devi_device_type', $item->devi_device_type);
			$stmt->execute();
			$db = null;
		} catch(PDOException $e) {
	        echo '{"error":';
				echo '{"text":'. $e->getMessage() .'}';
				echo '{"SQL":'.$sql.'}';
			echo '}';	
		}
	}
	function setEPGReminder() {
		$authUser = authUserId();
		$request = \Slim\Slim::getinstance()->request();
		$body = $request->getBody();
		$item = json_decode($body);	

		$type = 0;
		$entityId = $item->epg_epg_id;
		$status = $item->epg_reminder_status;
		$date = $item->epg_start;
		
		$setNotification = setNotifcation($authUser, $type, $entityId, $status, $date);
		
		$epgObj = getEPGById($entityId);
		
		return $epgObj;
	}	
	function avatarUpload() {
		
		$now = microtime(true);
		$authUser = authUserId();
		
		if (!class_exists('S3'))require_once('Slim/S3.php');
		if (!defined('awsAccessKey')) define('awsAccessKey', 'AKIAJJGGCPS4ZSBKG7XA');
		if (!defined('awsSecretKey')) define('awsSecretKey', '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');
		$s3 = new S3(awsAccessKey, awsSecretKey);
		
		$fileName = $_FILES['avatar']['name'];
		$fileTempName = $_FILES['avatar']['tmp_name'];
		$ext = pathinfo($fileName, PATHINFO_EXTENSION);
		$updateFileName = $authUser.'-'.rand_string(24).'.'.$ext;
		
		if ($s3->putObjectFile($fileTempName, "files2s", 'avatars/'.$updateFileName, S3::ACL_PUBLIC_READ)) {
			$db = getConnection();
			$userAvatar = $db->exec("UPDATE `app_user` SET `user_avatar_url`='$updateFileName', `user_updated`='$now' WHERE `user_user_id`='$authUser'");
			$db = null;
			echo '{"avatarUrl":'.json_encode($updateFileName).'}';	
		}else{
			print_r(E_ALL);
		}	
		
	}
	// Private Functions
	function rand_string( $length ) {
		$chars = "abcdefghijklmnopqrstuvwxyz0123456789";	
		$str = '';
		
		$size = strlen( $chars );
		for( $i = 0; $i < $length; $i++ ) {
			$str .= $chars[ rand( 0, $size - 1 ) ];
		}
	
		return $str;
	}	
	function updateUserInfo() {
		$now = microtime(true);
		$authUser = authUserId();
		$request = \Slim\Slim::getinstance()->request();
		$body = $request->getBody();
		$item = json_decode($body);
		$field = $item->field;
		
		$sql = "UPDATE `app_user` SET `$field`=:value, `user_updated`='$now' WHERE `user_user_id`='$authUser'";
		try {
			$db = getConnection();
			$stmt = $db->prepare($sql);
			$stmt->bindParam(":value", $item->value);
			$stmt->execute();
			$usrObj = $db->query("SELECT * FROM `app_user` WHERE `user_user_id`='$authUser'")->fetch(PDO::FETCH_OBJ);
			echo json_encode($usrObj);
			$db = null;
		} catch(PDOException $e) {
	        echo '{"error":';
				echo '{"text":'. $e->getMessage() .'}';
				echo '{"SQL":'.$sql.'}';
			echo '}';		
		}				
				
	}
	function addToWatchList() {
		$now = microtime(true);
		$authUser = authUserId();
		
		$request = \Slim\Slim::getinstance()->request();
		$body = $request->getBody();
		$item = json_decode($body);
		
		$episodeId = $item->wali_episode_id;
		
		$sql = "INSERT INTO `app_watch_list` (`wali_user_id`, `wali_programme_id`, `wali_episode_id`, `wali_updated`, `wali_status`, `wali_is_series`) VALUES ('$authUser', :wali_programme_id, :wali_episode_id, '$now', :wali_status, :wali_is_series) ON DUPLICATE KEY UPDATE `wali_updated`='$now', `wali_status` =:wali_status;";
		try {
			$db = getConnection();
			$stmt = $db->prepare($sql);
			$stmt->bindParam(":wali_episode_id", $item->wali_episode_id);
			$stmt->bindParam(":wali_programme_id", $item->wali_programme_id);
			$stmt->bindParam(":wali_is_series", $item->wali_is_series);
			$stmt->bindParam(":wali_status", $item->wali_status);
			$stmt->execute();
			$waliId = $db->lastInsertId();
			//Get Expiry Date for Episode & Set Notification
			$episodeExpiry = $db->query("SELECT FROM_UNIXTIME(`epis_expiry`) FROM `meta_episodes` WHERE `epis_episode_id`='$episodeId' LIMIT 1")->fetchColumn();
			$setNotification = setNotifcation($authUser, 1, $episodeId, 1, $episodeExpiry);
			//End
			$waliObj = $db->query("SELECT * FROM `app_watch_list` WHERE `wali_watchlist_id`='$waliId'")->fetch(PDO::FETCH_OBJ);
			echo json_encode($waliObj);
			$db = null;
		} catch(PDOException $e) {
	        echo '{"error":';
				echo '{"text":'. $e->getMessage() .'}';
				echo '{"SQL":'.$sql.'}';
			echo '}';		
		}		
		
	}
	function setWatchStatus() {
		$now = microtime(true);
		$authUser = authUserId();
		
		$request = \Slim\Slim::getinstance()->request();
		$body = $request->getBody();
		$item = json_decode($body);
		$episId = $item->wast_episode_id;
		$status = $item->wast_status;
		
		$sql = "INSERT INTO `app_watch_status` (`wast_user_id`, `wast_episode_id`, `wast_date_watched`, `wast_status`) VALUES ('$authUser', :wast_episode_id, '$now', :wast_status) ON DUPLICATE KEY UPDATE `wast_date_watched`='$now', `wast_status`=:wast_status;	";	
		try {
			$db = getConnection();
			$stmt = $db->prepare($sql);
			$stmt->bindParam(":wast_episode_id", $item->wast_episode_id);
			$stmt->bindParam(":wast_status", $item->wast_status);
			$stmt->execute();
			$wastId = $db->lastInsertId();
			if($status == 1) {
				$trending = $db->exec("INSERT INTO `app_trending` (`tren_episode_id`, `tren_count`, `tren_updated`) VALUES ('$episId',1,'$now') ON DUPLICATE KEY UPDATE `tren_count`=`tren_count`+1, `tren_updated`='$now'");
			}
			$wastObj = $db->query("SELECT * FROM `app_watch_status` WHERE `wast_watchstatus_id`='$wastId'")->fetch(PDO::FETCH_OBJ);			
			echo json_encode($wastObj);
			$db = null;
		} catch(PDOException $e) {
	        echo '{"error":';
				echo '{"text":'. $e->getMessage() .'}';
				echo '{"SQL":'.$sql.'}';
			echo '}';		
		}		
		
	}
	function setWatchListIndex() {
		$now = microtime(true);
		$authUser = authUserId();
		
		$request = \Slim\Slim::getinstance()->request();
		$body = $request->getBody();
		$item = json_decode($body);

		$error = null;

		foreach($item->watchlist as $obj) {
			$waliId = $obj->wali_watchlist_id;
			$waliIdx = $obj->wali_index;
			$sql = "UPDATE `app_watch_list` SET `wali_index`=:wali_index, `wali_updated`='$now' WHERE `wali_watchlist_id`=:wali_watchlist_id AND `wali_user_id`='$authUser'";
			try {
				$db = getConnection();
				$stmt = $db->prepare($sql);
				$stmt->bindParam(":wali_watchlist_id", $waliId);
				$stmt->bindParam(":wali_index", $waliIdx);
				$stmt->execute();
				$db = null;
			} catch(PDOException $e) {
				$error = $e;
				break;
			}			
		}
		if($error==null) {
			echo '{"success":true}';	
		}
	}
	function setAdditionalChannels(){
		$now = microtime(true);
		$authUser = authUserId();
		
		$request = \Slim\Slim::getinstance()->request();
		$body = $request->getBody();
		$item = json_decode($body);
		
		$sql = "UPDATE `app_user` SET `user_additional_channels`=:channels, `user_updated`='$now' WHERE `user_user_id`='$authUser'";
		try {
			$db = getConnection();
			$stmt = $db->prepare($sql);
			$stmt->bindParam(":channels", $item->user_additional_channels);
			$stmt->execute();
			$userObj = $db->query("SELECT * FROM `app_user` WHERE `user_user_id`='$authUser'")->fetch(PDO::FETCH_OBJ);			
			echo json_encode($userObj);
			$db = null;
		} catch(PDOException $e) {
	        		echo '{"error":';
				echo '{"text":'. $e->getMessage() .'}';
				echo '{"SQL":'.$sql.'}';
			echo '}';		
		}
	}

	function thisIsATest($name) {
		echo '{"name":"'.$name.'"}';
	}

	// PDO Function
	function getConnection() {
		$dbhost 		= "db.2ndscreen.tv";//NEW
		$dbuser 		= "SecondScreen";
		$dbpass 		= "9pEPstpHdfe9WTy";
		$dbname			= "SecondScreen";
		$options 		= array(
		    PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
		); 		
	    $dbh = new PDO("mysql:host=$dbhost;dbname=$dbname", $dbuser, $dbpass, $options);
	    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	    return $dbh;			
	}
	// Helpers
	$app->run();
?> 