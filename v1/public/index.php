<?php
	// Public API for Second Screen v1.0
	//
	// Author: Create Digital Media Ltd :: Justin Erswell
	// Date: 24th January 2014 
	
	require 'Slim/Slim.php';
	define(SALT, random_string(48));	
	
	date_default_timezone_set('Europe/London'); 
	\Slim\Slim::registerAutoloader();
	$app = new \Slim\Slim();
	$app->contentType('application/json; charset=utf8mb4');
	
	// GET route
	$app->get('/checkEmail/:email', 'checkEmail');
	$app->get('/fbCheck/:tkn', 'fbCheck');
	$app->get('/forgotPassword/:email', 'forgotPassword');
	// POST Route
	$app->post('/registerUser/', 'registerUser');
	// Private Functions
	function passwordEmail($email, $hash) {
		$to      = $email;
		// subject
		$subject = 'Password Reset Request';
		// message
		$message = '
			<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"> 
			<html xmlns="http://www.w3.org/1999/xhtml">
			<head>
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
				<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
				<title>Forgot your password?</title>
			</head>
			<body>
			<table cellpadding="0" cellspacing="0" border="0" id="backgroundTable">
				<tr>
					<td valign="top"> 
					<table cellpadding="0" cellspacing="0" border="0" align="left">
						<tr>
							<td width="20" valign="top">&nbsp;</td>
							<td valign="top">&nbsp;</td>
							<td width="20" valign="top">&nbsp;</td>
						</tr>
						<tr>
							<td width="20" valign="top">&nbsp;</td>
							<td valign="top">
								<p style="color: #AAA; font-size:14px;">Second Screen</p>
								<h2>Reset your password</h2>
								<p>We\'ve just received a password reset request for your Second Screen account.</p>
							</td>
							<td width="20" valign="top">&nbsp;</td>
						</tr>
						<tr><td>&nbsp;</td></tr>
						<tr>
							<td width="20" valign="top">&nbsp;</td>
							<td valign="top">
								<p><strong>To reset your password simply click the following link:</strong><br />
									<a href="http://2ndscreen.tv/iforgot.php?f='.$hash.'" target ="_blank" title="Forgot Password" style="color: #527CB7; text-decoration: none;">http://2ndscreen.tv/iforgot.php?f='.$hash.'</a>
								</p>
							</td>
							<td width="20" valign="top">&nbsp;</td>
						</tr>
						<tr><td>&nbsp;</td></tr>
						<tr>
							<td width="20" valign="top">&nbsp;</td>
							<td valign="top">
								<p>If you didn\'t request this change, just ignore and delete this email. Your account will be safe.</p>
								<p>Thanks,</p>
								<p>The Second Screen Team.</p>
							</td>
							<td width="20" valign="top">&nbsp;</td>
						</tr>
						<tr><td>&nbsp;</td></tr>
						<tr>
							<td width="20" valign="top">&nbsp;</td>
							<td valign="top" style="border-top:1px dotted #CCC;">
								<p style="color: #AAA; font-size:12px;"><strong>Still can\'t log in?</strong> If you\'re still having problems logging in or if you think someone is trying you access your account, please <a href="mailto:admin@2ndscreen.tv" target ="_blank" title="Contact Us" style="color: #527CB7; text-decoration: none;">contact us</a>.</p>
							</td>
							<td width="20" valign="top">&nbsp;</td>
						</tr>
				   </table> 
					
					</td>
				</tr>
			
			
			</table>  
			<!-- End of wrapper table -->
			</body>
			</html>
		';
	
		// To send HTML mail, the Content-type header must be set
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
	
		// Additional headers
		$headers .= 'From: Second Screen <no-reply@2ndscreen.tv>' . "\r\n";
		$headers .= 'X-Mailer: PHP/'.phpversion();
		// Mail it
		mail($to, $subject, $message, $headers);		
	}
	// GET Functions
	function checkEmail($email, $src=0){
		$sql = "SELECT `user_email` FROM `app_user` WHERE `user_email`=:email LIMIT 1";
		try{
	        $db = getConnection();
	        $stmt = $db->prepare($sql);
	        $stmt->bindParam(':email', $email);
	        $stmt->execute();
	        $res = $stmt->rowCount();
	        if($res==1 && $src ==0){
				echo '{"exists":true, "message":"User already registered"}';
	        } elseif($res==1 && $src==1) {
	           	return '{"exists":true, "message":"User already registered"}';
			} elseif($res==0 && $src==0) {
	        	echo '{"exists":false, "message":"User not found"}';
	        } elseif($res==0 && $src==1) {
	        	return '{"exists":false, "message":"User not found"}';
	        }
	        $db = null;
		} catch(PDOException $e) {
			echo '{"error":{"text":'.$e->getMessage().'}}';
			$db = null;
		}        

	}
	function fbCheck($tkn) {
		//Facebook Data
		$fbJSON = file_get_contents('https://graph.facebook.com/me/?access_token='.$tkn);
		$fbREST = json_decode($fbJSON);
		//echo $fbJSON;		

		$fbUserId = $fbREST->id;
		$fbUserEmail = $fbREST->email;
		$fbFirstName = $fbREST->first_name;
		$fbLastName = $fbREST->last_name;
		$fbGender = $fbREST->gender;
		$fbUserPassword = SALT;
		$fbUserAccountType = 1;
			
		//Checks
		$db = getConnection();
		$userWithEmail = $db->query("SELECT `user_user_id` FROM `app_user` WHERE `user_email`='$fbUserEmail'")->fetch(PDO::FETCH_OBJ);
		$db = null;
		$now = microtime(true);
		if(isset($userWithEmail->user_user_id)) {
			$uid = $userWithEmail->user_user_id;		
		} 
		
		if(isset($uid)) {
			//Update & Return User Object
			$sql = "UPDATE `app_user` SET `user_fb_id`=:fbid, `user_last_login`=:now WHERE `user_user_id`=:uid";
			try{
		        $db = getConnection();
		        $stmt = $db->prepare($sql);
		        $stmt->bindParam(':uid', $uid);
		        $stmt->bindParam(':now', $now);
		        $stmt->bindParam(':fbid', $fbUserId);
		        $stmt->execute();
		        //$avatar = avatarUpload($fbUserId, $uid);
				$usr = $db->query("SELECT `user_account_type`, `user_avatar_url`, `user_email`, `user_first_name`, `user_last_name`, `user_gender`, `user_user_id`, `user_password`, `user_first_registered`, `user_additional_channels` FROM `app_user` WHERE `user_user_id`='$uid'")->fetch(PDO::FETCH_OBJ);
		        echo json_encode($usr);
		        $db = null;
			} catch(PDOException $e) {
				echo '{"error":{"text":'.$e->getMessage().'}}';
				$db = null;
			}  				
		} else {
			//Create User and Return User Object
			$sql = "INSERT INTO `app_user` (`user_email`, `user_password`, `user_deleted`, `user_updated`, `user_first_name`, `user_last_name`, `user_gender`, `user_account_type`, `user_first_registered`, `user_last_login`, `user_fb_id`, `user_additional_channels`) VALUES (:user_email, :user_password, 0, '$now', :user_first_name, :user_last_name, :user_gender, :user_account_type, '$now', '$now', :user_fb_id, '')";	
			try {
				$db = getConnection();
				$stmt = $db->prepare($sql);
				$stmt->bindParam(":user_email", $fbUserEmail);
				$stmt->bindParam(":user_password", $fbUserPassword);
				$stmt->bindParam(":user_first_name", $fbFirstName);
				$stmt->bindParam(":user_last_name", $fbLastName);
				$stmt->bindParam(":user_gender", $fbGender);
				$stmt->bindParam(":user_account_type", $fbUserAccountType);
				$stmt->bindParam(":user_fb_id", $fbUserId);
				$stmt->execute();
				$uid = $db->lastInsertId();
				$avatar = avatarUpload($fbUserId, $uid);		
				$userObj = $db->query("SELECT * FROM `app_user` WHERE `user_user_id`='$uid'")->fetch(PDO::FETCH_OBJ);
				echo json_encode($userObj);
				$db = null;
			} catch(PDOException $e) {
		        echo '{"error":';
					echo '{"text":'. $e->getMessage() .'}';
					echo '{"SQL":'.$sql.'}';
				echo '}';		
			}
			
		}
		
	}
	function forgotPassword($email) {
		$sql = "SELECT `user_unique_string` FROM `app_user` WHERE `user_email`=:email LIMIT 1";
		try{
	        $db = getConnection();
	        $stmt = $db->prepare($sql);
	        $stmt->bindParam(':email', $email);
	        $stmt->execute();
	        $res = $stmt->rowCount();
			$hash = $stmt->fetchColumn();
	        if($res==1){
				passwordEmail($email, $hash);
				echo '{"exists":true, "message":"Password reset email sent to user"}';
	        } elseif($res==0) {
	        	return '{"exists":false, "message":"User not found"}';
	        }
	        $db = null;
		} catch(PDOException $e) {
			echo '{"error":{"text":'.$e->getMessage().'}}';
			$db = null;
		}        
	}	
	//POST Functions
	function registerUser() {
		$now = microtime(true);
		$request = \Slim\Slim::getinstance()->request();
		$body = $request->getBody();
		$user = json_decode($body);
		
		
		
		$checkJSON = checkEmail($user->user_email, $src=1);
		$checkREST = json_decode($checkJSON);
		if(!$checkREST->exists == false) {
			echo '{"exists":true, "message":"User already registered"}';
			return;
		}
		$sql = "INSERT INTO `app_user` (`user_email`, `user_password`, `user_deleted`, `user_updated`, `user_first_name`, `user_last_name`, `user_gender`, `user_account_type`, `user_first_registered`, `user_last_login`,`user_additional_channels`) VALUES (:user_email, :user_password, 0, '$now', :user_first_name, :user_last_name, :user_gender, :user_account_type, '$now', '$now', '')";	
		try {
			$db = getConnection();
			$stmt = $db->prepare($sql);
			$stmt->bindParam(":user_email", $user->user_email);
			$stmt->bindParam(":user_password", $user->user_password);
			$stmt->bindParam(":user_first_name", $user->user_first_name);
			$stmt->bindParam(":user_last_name", $user->user_last_name);
			$stmt->bindParam(":user_gender", $user->user_gender);
			$stmt->bindParam(":user_account_type", $user->user_account_type);	
			$stmt->execute();
			$uid = $db->lastInsertId();
			$userObj = $db->query("SELECT * FROM `app_user` WHERE `user_user_id`='$uid'")->fetch(PDO::FETCH_OBJ);			
			echo json_encode($userObj);
			$db = null;
		} catch(PDOException $e) {
	        echo '{"error":';
				echo '{"text":'. $e->getMessage() .'}';
				echo '{"SQL":'.$sql.'}';
			echo '}';		
		}
	} 
	// Private Functions
	function avatarUpload($fid, $uid) {
		
		$now = microtime(true);
		
		if (!class_exists('S3'))require_once('Slim/S3.php');
		if (!defined('awsAccessKey')) define('awsAccessKey', 'AKIAJJGGCPS4ZSBKG7XA');
		if (!defined('awsSecretKey')) define('awsSecretKey', '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');
		$s3 = new S3(awsAccessKey, awsSecretKey);
		
		$img = file_get_contents('https://graph.facebook.com/'.$fid.'/picture?type=large');
		$file = dirname(__file__).'/tmp/'.$fid.'.jpg';
		file_put_contents($file, $img);
		
		$ext = pathinfo($file, PATHINFO_EXTENSION);
		$updateFileName = $uid.'-'.rand_string(24).'.'.$ext;
		
		if ($s3->putObjectFile($file, "files2s", 'avatars/'.$updateFileName, S3::ACL_PUBLIC_READ)) {
			$db = getConnection();
			$userAvatar = $db->exec("UPDATE `app_user` SET `user_avatar_url`='$updateFileName', `user_updated`='$now' WHERE `user_user_id`='$uid'");
			$db = null;
		}else{
			print_r(E_ALL);
		}	
		
	}
	function rand_string( $length ) {
		$chars = "abcdefghijklmnopqrstuvwxyz0123456789";	
		$str = '';
		
		$size = strlen( $chars );
		for( $i = 0; $i < $length; $i++ ) {
			$str .= $chars[ rand( 0, $size - 1 ) ];
		}
	
		return $str;
	}
	function random_string($length) {
	    $key = '';
	    $keys = array_merge(range(0, 9), range('a', 'z'));
	
	    for ($i = 0; $i < $length; $i++) {
	        $key .= $keys[array_rand($keys)];
	    }
	
	    return $key;
	}	
	// PDO Function
	function getConnection() {
		$dbhost 		= "db.2ndscreen.tv";//NEW
		$dbuser 		= "SecondScreen";
		$dbpass 		= "9pEPstpHdfe9WTy";
		$dbname			= "SecondScreen";
		$options 		= array(
		    PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
		); 		
	    $dbh = new PDO("mysql:host=$dbhost;dbname=$dbname", $dbuser, $dbpass, $options);
	    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	    return $dbh;		
	}
	
	$app->run();
?>